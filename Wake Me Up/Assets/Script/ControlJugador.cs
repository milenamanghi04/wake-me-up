using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{

    //MOVIMIENTO Y SALTO
    public int speed;
    public float forceJump;
    private Rigidbody rb;
    private Vector3 movementInput;

    //CONTROLGRAVEDAD
    private bool derecha;
    private bool abajo;
    private bool izquierda;
    private bool arriba;

    //CONTROLCAMARA
     public Camera camera;
     public float cameraSpeed = 0f;
     public Vector3 offset;

    void Start()
    {
        izquierda = false;
        derecha = false;
        arriba = false;
        abajo = true;
    }

    void Awake()
    {
        rb = GetComponent<Rigidbody>();

    }

    void Update()
    {
        movementInput = Vector3.zero;
        if(abajo == true)
        {
          movimientoAbajo();
        }
        else if( derecha == true)
        {
            movimientoDerecha();
        }
        else if(izquierda == true)
        {
            movimientoIzquierda();
        }
        else if(arriba == true)
        {
            movimientoArriba();
        }
   

        Move(movementInput);
        Jump();


    }

    void FixedUpdate()
    {
        
    
        
    }

    void LateUpdate()
    {
        camera.transform.position = this.transform.position + offset;

        if (abajo == true)
        {
            offset = new Vector3(0, 1, -3);
        }
        else if (arriba == true)
        {
            offset = new Vector3(0, -1f, -3);
        }
        
    }

    void movimientoAbajo()
    {
        if (Input.GetKey(KeyCode.W))
        {
            movementInput.z = 1;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            movementInput.z = -1;
        }
        if (Input.GetKey(KeyCode.D))
        {
            movementInput.x = 1;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            movementInput.x = -1;
        }
    }

    void movimientoDerecha()
    {
        if (Input.GetKey(KeyCode.W))
        {
            movementInput.z = 1;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            movementInput.z = -1;
        }
        if (Input.GetKey(KeyCode.D))
        {
            movementInput.y = 1;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            movementInput.y = -1;
        }
    }

    void movimientoArriba()
    {
        if (Input.GetKey(KeyCode.W))
        {
            movementInput.z = 1;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            movementInput.z = -1;
        }
        if (Input.GetKey(KeyCode.D))
        {
            movementInput.x = -1;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            movementInput.x = 1;
        }

    }

    void movimientoIzquierda()
    {
        if (Input.GetKey(KeyCode.W))
        {
            movementInput.z = 1;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            movementInput.z = -1;
        }
        if (Input.GetKey(KeyCode.D))
        {
            movementInput.y = -1;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            movementInput.y = 1;
        }
    }


    void Jump()
    {

        if(Input.GetKeyDown(KeyCode.Space))
        {

            if(abajo==true)
            {
              rb.AddForce(Vector2.up * forceJump, ForceMode.Impulse);
            }
            else if (derecha == true)
            {
               rb.AddForce( new Vector2(-1,0) * forceJump, ForceMode.Impulse);

            }
            else if (izquierda == true)
            {
                rb.AddForce(new Vector2(1, 0) * forceJump, ForceMode.Impulse);
            }
            else if (arriba == true)
            {
                rb.AddForce(Vector2.down * forceJump, ForceMode.Impulse);
            }




        }

    }
  
    void Move(Vector3 direction)
    {
        rb.AddForce(direction.normalized * speed, ForceMode.Acceleration);
        
    }


    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "ParedDerecha")
        {
            Physics.gravity = new Vector3(9, 0, 0);
            camera.transform.rotation = collision.transform.rotation;
            derecha = true;
            abajo = false;
            arriba = false;
            izquierda = false;
        }

        if(collision.gameObject.tag == "Suelo")
        {
            Physics.gravity = new Vector3(0, -9, 0);
            camera.transform.rotation = collision.transform.rotation;
            derecha = false;
            abajo = true;
            arriba = false;
            izquierda = false;


        }

        if (collision.gameObject.tag == "ParedIzquierda")
        {
            Physics.gravity = new Vector3(-9, 0, 0);
            camera.transform.rotation = collision.transform.rotation;
            derecha = false;
            abajo = false;
            arriba = false;
            izquierda = true;

        }
        if (collision.gameObject.tag == "Techo")
        {
            Physics.gravity = new Vector3(0, 9, 0);
            camera.transform.rotation = collision.transform.rotation;
            derecha = false;
            abajo = false;
            arriba = true;
            izquierda = false;

        }


    }


}
